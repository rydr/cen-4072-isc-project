
/*****************************************************************************

   Create `mydb` Database
   Reference Storage.Repository.DBConnection
   
 ******************************************************************************/
SELECT 'DROP and CREATE DATABSE mydb' AS '';
DROP DATABASE IF EXISTS mydb;
CREATE DATABASE mydb;


/*****************************************************************************

   Create user for Database and Grant Privileges
   Reference Storage.Repository.DBConnection
   
 ******************************************************************************/

SELECT 'CREATE USER \'admin\'@\'localhost\' and GRANT PRIVELEGES' AS '';
DROP USER 'admin'@'localhost';
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'cen4010';
GRANT ALL PRIVILEGES ON mydb.* TO 'admin'@'localhost';
FLUSH PRIVILEGES;


/*****************************************************************************

   Create `mydb` Tables
   
 ******************************************************************************/

use mydb;


SELECT 'CREATE TABLE panel...' AS '';
DROP TABLE IF EXISTS panel;
CREATE TABLE panel (
    PanelID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(45),
    Description TEXT
);


SELECT 'CREATE TABLE status...' AS '';
DROP TABLE IF EXISTS status;
CREATE TABLE status (
    StatusID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Type VARCHAR(45)
);


SELECT 'CREATE TABLE panelhistory' AS '';
DROP TABLE IF EXISTS panelhistory;
CREATE TABLE panelhistory (
    PanelID INT UNSIGNED AUTO_INCREMENT,
    PanelStatus VARCHAR(45),
    StatusDate DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    Comments TEXT,
    FOREIGN KEY (PanelID) REFERENCES panel(PanelID)
    /* FOREIGN KEY (PanelStatus) REFERENCES status(StatusID) */
);


SELECT 'CREATE TABLE iscusers...' AS '';
DROP TABLE IF EXISTS iscusers;
CREATE TABLE iscusers (
    ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Username VARCHAR(45),
    Password VARCHAR(45),
    FirstName VARCHAR(45),
    LastName VARCHAR(45),
    Locked VARCHAR(45),
    UserType INT
);


SELECT 'CREATE TABLE iscemployee...' AS '';
DROP TABLE IF EXISTS iscemployee;
CREATE TABLE iscemployee (
    EID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    UserID INT UNSIGNED,
    Type TINYINT,
    FOREIGN KEY (UserID) REFERENCES iscusers(ID)
);


SELECT 'CREATE TABLE iscids...' AS '';
DROP TABLE IF EXISTS iscids;
CREATE TABLE iscids (
    iscid INT UNSIGNED PRIMARY KEY
);


SELECT 'CREATE TABLE iscpanelist...' AS '';
DROP TABLE IF EXISTS iscpanelist;
CREATE TABLE iscpanelist (
    ISCID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    UserID INT UNSIGNED NOT NULL,
    Institution VARCHAR(45),
    Address VARCHAR(45),
    City VARCHAR(45),
    State VARCHAR(45),
    ZipCode VARCHAR(45),
    Telephone VARCHAR(45),
    Email VARCHAR(45),
    Gender INT,
    Ethnicity VARCHAR(45)
    #FOREIGN KEY (UserID) REFERENCES iscids(iscid)
);


SELECT 'CREATE TABLE expertise...' AS '';
DROP TABLE IF EXISTS expertise;
CREATE TABLE expertise (
    ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(45)
);


SELECT 'CREATE TABLE panelistexpertise...' AS '';
DROP TABLE IF EXISTS panelistexpertise;
CREATE TABLE panelistexpertise (
    pid INT UNSIGNED NOT NULL,
    expertiseid INT UNSIGNED NOT NULL,
    FOREIGN KEY (pid) REFERENCES iscpanelist(ISCID),
    FOREIGN KEY (expertiseid) REFERENCES expertise(ID)
);


SELECT 'CREATE TABLE servingpanelists...' AS '';
DROP TABLE IF EXISTS servingpanelists;
CREATE TABLE servingpanelists (
    PanelistID INT UNSIGNED NOT NULL,
    PanelID INT UNSIGNED NOT NULL,
    FOREIGN KEY (PanelistID) REFERENCES iscpanelist(ISCID),
    FOREIGN KEY (PanelID) REFERENCES panel(PanelID)
);


SELECT 'CREATE TABLE panelaccess...' AS '';
DROP TABLE IF EXISTS panelaccess;
CREATE TABLE panelaccess (
    EmployeeID INT UNSIGNED NOT NULL,
    PanelID INT UNSIGNED NOT NULL,
    FOREIGN KEY (EmployeeID) REFERENCES iscemployee(EID),
    FOREIGN KEY (PanelID) REFERENCES panel(PanelID)
);


/*****************************************************************************

   Initialize Required Data Rows
   
 ******************************************************************************/

/*
 	Create some usable iscids for registering panelists
 	Reference PanelistContainer.createPanelist
 */
SELECT 'INSERT INTO iscids' AS '';
INSERT INTO iscids (iscid)
	VALUES (999),(1000),(1001),(1002),(1003),(1004),(1005);

/*
 	forms/register.html specifies values which are inserted by
 	PanelistContainer.createPanelist(...)
 */ 
SELECT 'INSERT INTO expertise...' AS '';
INSERT INTO expertise (ID, Name)
    VALUES 
        (1, 'Computer Science'),
        (2, 'Civil Engineering'),
        (3, 'Electrical Engineering'),
        (4, 'Computer Engineering'),
        (5, 'Physics');


/*****************************************************************************

   Initialize Sample Data Rows
   
 ******************************************************************************/

/* 
 	Create a user of each type
    Specify ID to simplify creation of references in other tables
    Reference Storage.Repository.UserType for UserType values
 */
SELECT 'INSERT INTO iscusers...' AS '';
INSERT INTO iscusers (ID, Username, Password, FirstName, LastName, Locked, UserType)
	VALUES
		(1, 'admin', 'admin', 'Peter', 'Clarke', 0, 2),
		(2, 'employee', 'employee', 'John', 'Doe', 0, 1),
		(3, 'panelist', 'panelist', 'Jane', 'Doe', 0, 0);

/*
 	Insert Required Employee Data
 */
SELECT 'INSERT INTO iscemployee...' AS '';
INSERT INTO iscemployee (EID, UserID, Type)
	VALUES
		(1, 2, 1);

/*
 	Register Panelist
 	Using basic user from iscuser INSERT
 	Reference PanelistContainer.createPanelist
 */
SELECT 'Panelist Data, INSERT INTO (iscpanelist|panelistexpertise)' AS '';
INSERT INTO iscpanelist (ISCID, UserID, Institution, Address, City, State, ZipCode, Telephone, Email, Gender, Ethnicity)
	VALUES (999, 3, 'FIU', '123 Fiu Lane', 'Miami', 'FL', '33127', '1234567890', 'jane@doe.com', 0, 'all');
INSERT INTO panelistexpertise (pid, expertiseid)
	VALUES (999, 5);

/*
   Create a panel
   Reference PanelContainer.createPanel(...)
 */
SELECT 'Create some panels...' AS '';
INSERT INTO panel(PanelID, Name, Description)
	VALUES
		(1, 'Algorithms', 'Best Algorithms by Computer Scientists.'),
		(2, 'FIU Panel', 'All about FIU');
INSERT INTO panelhistory (PanelID, PanelStatus, Comments)
	VALUES
		(1, 'In Progress', 'Panel Created'),
		(2, 'In Progress', 'Panel Created');
INSERT INTO panelaccess (EmployeeID, PanelID)
	VALUES
		(1, 1),
		(1, 2);

/*
	Add Panelist to Panel
 */
SELECT 'Add Panelist to Panel...' AS '';
INSERT INTO servingpanelists (PanelistID, PanelID)
	VALUES
		(999, 2);
