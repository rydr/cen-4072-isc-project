

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import Storage.DBManagerAddPanelistToPanelTest;
import Storage.DBManagerCreatePanelTest;
import Storage.DBManagerCreatePanelistTest;
import Storage.DBManagerGetPanelistTest;
import Storage.DBManagerGetPanelsTest;
import Storage.DBManagerUpdatePanelStatusTest;
import Storage.DBManagerValidateUsernameTest;

@RunWith( Suite.class )
@SuiteClasses( {
    DBManagerCreatePanelistTest.class, DBManagerGetPanelistTest.class, DBManagerValidateUsernameTest.class,
    DBManagerCreatePanelTest.class, DBManagerAddPanelistToPanelTest.class, DBManagerUpdatePanelStatusTest.class,
    DBManagerGetPanelsTest.class
} )
public class DBManagerTestSuite {

}
