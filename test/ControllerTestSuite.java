import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ApplicationLogic.ControllerCreatePanelistAccountTest;
import ApplicationLogic.ControllerDoGetTest;
import ApplicationLogic.ControllerDoLoginTest;
import ApplicationLogic.ControllerDoLogoutTest;
import ApplicationLogic.ControllerDoPostTest;
import ApplicationLogic.ControllerSearchPanelistsTest;

/**
 * @note that tests were not created for the following Controller methods:
 *       createPanel, addPanelistToPanel, updatePanelStatus, getPanel, and
 *       getEmployeePanels. The reasoning for this is that the these methods are
 *       facade methods that directly call methods in the PanelManager class
 *       which have already been tested.
 */

@RunWith( Suite.class )
@SuiteClasses({
    ControllerCreatePanelistAccountTest.class, ControllerDoGetTest.class, ControllerDoLoginTest.class,
    ControllerDoLogoutTest.class, ControllerDoPostTest.class, ControllerSearchPanelistsTest.class
})
public class ControllerTestSuite {

}
