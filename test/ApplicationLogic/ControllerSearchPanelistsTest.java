package ApplicationLogic;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import edu.fiu.test.Fixtures.PanelistProfileFixture;

import Storage.DBManager;
import Storage.Repository.PanelistProfile;

/**
 * == Controller.searchPanelists Test Cases ==
 * 
 * <dt><b>Note:</b></dt> Based on testing results for DBManager.getPanelists
 * {@link DBManagerGetPanelistsTest}, it is expected to return an
 * ArrayList<PanelistProfile> that will either be empty or contain profiles
 * matching the criteria.
 */

@PrepareForTest( { DBManager.class } )
public class ControllerSearchPanelistsTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private HttpServletRequest mRequest;
    private HttpServletResponse mResponse;
    private HttpSession mSession;
    ArrayList<PanelistProfile> datapool;

    @Before
    public void setUp () throws Exception {
        PowerMockito.mockStatic( DBManager.class );

        mRequest = mock( HttpServletRequest.class );
        mResponse = mock( HttpServletResponse.class );
        mSession = mock( HttpSession.class );

        datapool = new ArrayList<>();
    }

    /**
     * <dt><b>Test Case UTC-48/CON-401</b></dt>
     *
     * Verify that given the search criteria are valid and produce results and
     * the Storage subsystem does not encounter any errors, then the found
     * PanelistProfile objects should be stored in the session.
     * 
     * <dt><b>Note:</b></dt> that even though it is standard practice to use a
     * RequestDispatcher to forward the objects to the correct page
     * (displayPanelists.jsp), we will test using the strategy that has been
     * used throughout the project (sendRedirect) despite the sequence diagram
     * inferring a direct response. Using the redirect strategy requires
     * storing the results somewhere, in this case the session.
     */
    @SuppressWarnings( "unchecked" )
    @Test
    public final void searchPanelists_ValidCriteriaWithResults_SavesResultsInSession () throws Exception {
        /* Arrange */
        PanelistProfileFixture.defaultProfileCollection( 5, datapool );
        when( DBManager.getPanelists( (HashMap<String, String>) anyMap() ) ).thenReturn( datapool );

        /* Act */
        Controller.searchPanelists( mRequest, mResponse, mSession );

        /* Assert */
        verify( mSession ).setAttribute( "Panelists", datapool );
    }

    /**
     * <dt><b>Test Case UTC-49/CON-402</b></dt>
     *
     * Verify that given the search criteria are valid and produce results and
     * the Storage subsystem does not encounter any errors, then the request
     * response should redirect to the displayPanelists.jsp page.
     */
    @SuppressWarnings( "unchecked" )
    @Test
    public final void searchPanelists_ValidCriteriaWithResults_RedirectsToPanelistPage () throws Exception {
        /* Arrange */
        PanelistProfileFixture.defaultProfileCollection( 5, datapool );
        when( DBManager.getPanelists( (HashMap<String, String>) anyMap() ) ).thenReturn( datapool );

        /* Act */
        Controller.searchPanelists( mRequest, mResponse, mSession );

        /* Assert */
        verify( mResponse ).sendRedirect( "displayPanelists.jsp" );
    }

    /**
     * <dt><b>Test Case UTC-50/CON-403</b></dt>
     *
     * Verify that given the search criteria are valid and do _not_ produce any
     * results and the Storage subsystem does not encounter errors, then the
     * request response should redirect to the error page with an error
     * message.
     */
    @SuppressWarnings( "unchecked" )
    @Test
    public final void searchPanelists_ValidCriteriaWithNoResults_RedirectsToErrorMessagePage () throws Exception {
        /* Arrange */
        PanelistProfileFixture.defaultProfileCollection( 0, datapool );
        when( DBManager.getPanelists( (HashMap<String, String>) anyMap() ) ).thenReturn( datapool );

        /* Act */
        Controller.searchPanelists( mRequest, mResponse, mSession );

        /* Assert */
        verify( mResponse ).sendRedirect( "messagePage?messageCode=No Panelists Found." );
    }

}
