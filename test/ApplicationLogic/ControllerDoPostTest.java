package ApplicationLogic;

import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

/**
 * == Controller.doPost Test Cases ==
 * 
 * <dt><b>Note:</b></dt> since the other methods have been unit tested and the
 * doPost method is an entry point to calling each of those methods, these
 * tests will only verify that the mapping is correct.
 */

@PrepareForTest({ PanelManager.class, SearchPanelist.class })
public class ControllerDoPostTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private Controller sController;
    private HttpServletRequest mRequest;
    private HttpServletResponse mResponse;
    private HttpSession mSession;

    @Before
    public void setUp () throws Exception {
        sController = spy( new Controller() );

        PowerMockito.mockStatic( PanelManager.class );
        PowerMockito.mockStatic( SearchPanelist.class );

        mRequest = mock( HttpServletRequest.class );
        mResponse = mock( HttpServletResponse.class );
        mSession = mock( HttpSession.class );

        when( mRequest.getSession( true ) ).thenReturn( mSession );
    }

    /**
     * <dt><b>Test Case UTC-51/CON-501</b></dt>
     *
     * Verify that given an "action-type" init param of "doLogin", then the
     * Controller.doLogin method should be called with the received request and
     * response objects and a new session object from the requesting session.
     */
    @Test
    public final void doPost_doLoginInitParam_callsDoLoginMethod () throws Exception {
        /* Arrange */
        doReturn( "doLogin" ).when(  sController ).getInitParameter( "action-type" );
        doNothing().when( sController )
            .doLogin( any( HttpServletRequest.class ), any( HttpServletResponse.class ), any( HttpSession.class) );

        /* Act */
        sController.doPost( mRequest, mResponse );

        /* Assert */
        verify( sController ).doLogin( mRequest, mResponse, mSession );
    }

    /**
     * <dt><b>Test Case UTC-52/CON-502</b></dt>
     *
     * Verify that given an "action-type" init param of
     * "createPanelistAccount", then the Controller.createPanelistAccount
     * method should be called with the received request and response objects.
     */
    @Test
    public final void doPost_createPanelistAccountInitParam_callsCreatePanelistAccountMethod () throws Exception {
        /* Arrange */
        doReturn( "createPanelistAccount" ).when(  sController ).getInitParameter( "action-type" );
        doNothing().when( sController )
            .createPanelistAccount( any( HttpServletRequest.class ), any( HttpServletResponse.class) );

        /* Act */
        sController.doPost( mRequest, mResponse );

        /* Assert */
        verify( sController ).createPanelistAccount( mRequest, mResponse );
    }

    /**
     * <dt><b>Test Case UTC-53/CON-503</b></dt>
     *
     * Verify that given an "action-type" init param of
     * "addToPanel", then the Controller.addPanelistToPanel method should be
     * called with the received request and response objects.
     */
    @Test
    public final void doPost_addToPanelInitParam_callsAddPanelistToPanelMethod () throws Exception {
        /* Arrange */
        //when( mController.getInitParameter( "action-type" ) ).thenReturn( "addToPanel" );
        doReturn( "addToPanel" ).when(  sController ).getInitParameter( "action-type" );

        /* Act */
        sController.doPost( mRequest, mResponse );

        /* Assert */
        PowerMockito.verifyStatic();
        PanelManager.addPanelistToPanel( mRequest, mResponse );
    }

    /**
     * <dt><b>Test Case UTC-54/CON-504</b></dt>
     *
     * Verify that given an "action-type" init param of "updatePanelStatus" ,
     * then the Controller.updatePanelStatus method should be called with the
     * received request and response objects.
     */
    @Test
    public final void doPost_updatePanelStatusInitParam_callsUpdatePanelStatusMethod () throws Exception {
        /* Arrange */
        doReturn( "updatePanelStatus" ).when(  sController ).getInitParameter( "action-type" );

        /* Act */
        sController.doPost( mRequest, mResponse );

        /* Assert */
        PowerMockito.verifyStatic();
        PanelManager.updatePanelStatus( mRequest, mResponse );
    }

    /**
     * <dt><b>Test Case UTC-55/CON-505</b></dt>
     *
     * Verify that given an "action-type" init param of "createPanel", then the
     * Controller.createPanel method should be called with the received request
     * and response objects and a new session object based on the request.
     */
    @Test
    public final void doPost_createPanelInitParam_callsCreatePanelMethod () throws Exception {
        /* Arrange */
        doReturn( "createPanel" ).when(  sController ).getInitParameter( "action-type" );

        /* Act */
        sController.doPost( mRequest, mResponse );

        /* Assert */
        PowerMockito.verifyStatic();
        PanelManager.createPanel( mRequest, mResponse, mSession );
    }

    /**
     * <dt><b>Test Case UTC-56/CON-506</b></dt>
     *
     * Verify that given an "action-type" init param of "doSearch", then the
     * Controller.searchPanelists method should be called with the received
     * request and response objects and a new session object based on the
     * request.
     */
    @Test
    public final void doPost_doSearchInitParam_callsSearchPanelistMethod () throws Exception {
        /* Arrange */
        doReturn( "doSearch" ).when(  sController ).getInitParameter( "action-type" );

        /* Act */
        sController.doPost( mRequest, mResponse );

        /* Assert */
        PowerMockito.verifyStatic();
        SearchPanelist.searchPanelists( mRequest, mResponse, mSession );
    }

    /**
     * <dt><b>Test Case UTC-58/CON-507</b></dt>
     *
     * Verify that given an "action-type" init param of "root", then nothing
     * should be called.
     */
    @Test
    public final void doPost_InvalidInitParam_doesNothing () throws Exception {
        /* Arrange */
        doReturn( "root" ).when( sController ).getInitParameter( "action-type" );

        /* Act */
        sController.doPost( mRequest, mResponse );

        /* Assert */
        verify( sController ).doPost(  mRequest, mResponse );
        verify( sController ).getInitParameter( "action-type" );
        verifyNoMoreInteractions( sController );
    }

}
