package ApplicationLogic;

import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import Storage.DBManager;
import Storage.Repository.EmployeeProfile;

@PrepareForTest( { DBManager.class } )
public class PanelManagerCreatePanelTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private HttpServletRequest mRequest;
    private HttpServletResponse mResponse;
    private HttpSession mSession;
    private EmployeeProfile mEmployeeProfile;

    private final String successPage = "messagePage";
    private final String successMessage = "?messageCode=Panel has been successfully created.";
    private final String errorPage = "errorPage";
    private final String errorMessage = "?errorCode=There was an error creating the panel. Please try again.";

    @Before
    public void setUp () throws Exception {
        PowerMockito.mockStatic( DBManager.class );

        mRequest = mock( HttpServletRequest.class );
        mResponse = mock( HttpServletResponse.class );
        mSession = mock( HttpSession.class );

        mEmployeeProfile = mock( EmployeeProfile.class );
        mEmployeeProfile.EmployeeID = 42;
        when( mSession.getAttribute( "User Profile" ) ).thenReturn( mEmployeeProfile );
    }

    /**
     * <dt><b>Test Case UTC-25/PM-101</b></dt>
     *
     * Verifies that given that the panel name is an empty
     * string, when a request to create a panel is made, the response is to
     * redirect to the error page with a message.
     */
    @Test
    public final void createPanel_EmptyPanelName_RedirectsToErrorPage () throws Exception {
        /* Arrange */
        when( mRequest.getParameter( "panelName" ) ).thenReturn( "" );
        when( mRequest.getParameter( "panelDescription" ) ).thenReturn( "some panel description" );

        /* Act */
        PanelManager.createPanel( mRequest, mResponse, mSession );

        /* Assert */
        verify( mResponse ).sendRedirect( errorPage + errorMessage );
    }

    /**
     * <dt><b>Test Case UTC-26/PM-102</b></dt>
     *
     * Verifies that given the panel description is an
     * empty string, when a request to create a panel is made, then the response
     * is to redirect to the error page with a message.
     */
    @Test
    public final void createPanel_EmptyPanelDescription_RedirectsToErrorPage () throws Exception {
        /* Arrange */
        when( mRequest.getParameter( "panelName" ) ).thenReturn( "somee panel name" );
        when( mRequest.getParameter( "panelDescription" ) ).thenReturn( "" );

        /* Act */
        PanelManager.createPanel( mRequest, mResponse, mSession );

        /* Assert */
        verify( mResponse ).sendRedirect( errorPage + errorMessage );
    }

    /**
     * <dt><b>Test Case UTC-27/PM-103</b></dt>
     *
     * Verifies that given the panel name is _not_ empty
     * and the panel description is _not_ empty and the Storage fails to create
     * the panel, then the response is to redirect to the error page with a
     * message.
     * 
     * <dt><b>Note:</b></dt> Based on testing results for DBManager.createPanel
     * {@link DBManagerCreatePanelTest}, the expected return values can be only
     * true or false.
     */
    @Test
    public final void createPanel_PanelNotSaved_RedirectsToErrorPage () throws Exception {
        /* Arrange */
        when( mRequest.getParameter( "panelName" ) ).thenReturn( "some panel name" );
        when( mRequest.getParameter( "panelDescription" ) ).thenReturn( "some panel description" );
        when( DBManager.createPanel( anyString(), anyString(), anyInt() ) ).thenReturn( false );

        /* Act */
        PanelManager.createPanel( mRequest, mResponse, mSession );

        /* Assert */
        verify( mResponse ).sendRedirect( errorPage + errorMessage );
    }

    /**
     * <dt><b>Test Case UTC-28/PM-104</b></dt>
     *
     * Verifies that given the panel name, panel
     * description, and employee id are valid and the Storage subsystem creates
     * the panel, then the request response will redirect to the message page
     * with a success message.
     */
    @Test
    public final void createPanel_ValidInputsAndSuccessfullySave_RedirectsToMessagePage () throws Exception {
        /* Arrange */
        when( mRequest.getParameter( "panelName" ) ).thenReturn( "some panel name" );
        when( mRequest.getParameter( "panelDescription" ) ).thenReturn( "some panel description" );
        when( DBManager.createPanel( anyString(), anyString(), anyInt() ) ).thenReturn( true );

        /* Act */
        PanelManager.createPanel( mRequest, mResponse, mSession );

        /* Assert */
        verify( mResponse ).sendRedirect( successPage + successMessage );
    }
}
