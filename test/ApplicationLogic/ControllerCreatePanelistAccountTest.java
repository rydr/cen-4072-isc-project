package ApplicationLogic;

import static org.mockito.Mockito.*;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import Storage.DBManager;

/**
 * == Controller.createPanelistAccount Test Cases ==
 * 
 * <dt><b>Note:</b></dt> Based on testing results for DBManager.createPanelist
 * {@link DBCreatePanelistTest}, the expected return values can be one of true,
 * false, or a DoubleRegistrationException.
 */

@PrepareForTest( { DBManager.class } )
public class ControllerCreatePanelistAccountTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private HttpServletRequest mRequest;
    private HttpServletResponse mResponse;

    @Before
    public void setUp () throws Exception {
        PowerMockito.mockStatic( DBManager.class );

        mRequest = mock( HttpServletRequest.class );
        mResponse = mock( HttpServletResponse.class );
    }

    /**
     * <dt><b>Test Case UTC-45/CON-301</b></dt>
     *
     * Verify that given the ISCID in the request parameters is valid and not
     * already registered and the Storage subsystem does not encounter an
     * error, then the request response should redirect to the message page
     * with a success message.
     */
    @SuppressWarnings( "unchecked" )
    @Test
    public final void createPanelistAccount_ValidISCIDNotRegistered_RedirectsToSuccessMessagePage () throws Exception {
        /* Arrange */
        when( DBManager.createPanelist( (HashMap<String, String>) anyMap() ) ).thenReturn( true );

        /* Act */
        new Controller().createPanelistAccount( mRequest, mResponse );

        /* Assert */
        verify( mResponse ).sendRedirect( "messagePage?messageCode=Registration Successful" );
    }

    /**
     * <dt><b>Test Case UTC-46/CON-302</b></dt>
     *
     * Verify that given the ISCID in the request parameters is not valid and
     * the Storage subsystem does not encounter and error, then the request
     * response is to redirect to the error page with an error message.
     */
    @SuppressWarnings( "unchecked" )
    @Test
    public final void createPanelistAccount_InvalidISCIDNotRegistered_RedirectsToErrorMessagePage () throws Exception {
        /* Arrange */
        when( DBManager.createPanelist( (HashMap<String, String>) anyMap() ) ).thenReturn( false );

        /* Act */
        new Controller().createPanelistAccount( mRequest, mResponse );

        /* Assert */
        verify( mResponse ).sendRedirect( "errorPage?errorCode=Registration failed. Please try again." );
    }

    /**
     * <dt><b>Test Case UTC-47/CON-303</b></dt>
     *
     * Verify that given the ISCID in the request parameters is valid and
     * _already_ registered and the Storage subsystem does not encounter an
     * error, then the request response should redirect to the error page with
     * a message stating the ISCID is already registered.
     */
    @SuppressWarnings( "unchecked" )
    @Test
    public final void createPanelistAccount_ValidISCIDAlreadyRegistered_RedirectToErrorMessagePage () throws Exception {
        /* Arrange */
        when( DBManager.createPanelist( (HashMap<String, String>) anyMap() ) )
            .thenThrow( Storage.Repository.DoubleRegistrationException.class );

        /* Act */
        new Controller().createPanelistAccount( mRequest, mResponse );

        /* Assert */
        verify( mResponse )
            .sendRedirect( "errorPage?errorCode=Double Registration Exception: ISCID is already in use." );
    }

}
