package ApplicationLogic;

import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;

/**
 * == Controller.doLogout Test Cases ==
 * 
 * <ul>
 * <li> User Manual, Page 20, Log Out</li>
 * <li> Final Document, Page 14, Log Out Functional Requirements</li>
 * <li> Final Document, Page 18, Log Out Non-Functional Requirements</li>
 * <li> Final Document, Page 44, Log Out Sequence Diagram</li>
 * <li> Final Document, Page 64, Use Case ISCUC-04 Log Out</li>
 * </ul>
 */

public class ControllerDoLogoutTest {

    private HttpServletResponse mResponse;
    private HttpSession mSession;

    @Before
    public void setUp () throws Exception {
        mResponse = mock( HttpServletResponse.class );
        mSession = mock( HttpSession.class );
        new Controller().doLogout( mResponse, mSession );
    }

    /**
     * <dt><b>Test Case UTC-43/CON-201</b></dt>
     *
     * Verify that given a valid session object, then the session should be
     * invalidated.
     */
    @Test
    public final void doLogout_ValidSession_InvalidatesSession () throws Exception {
        /* Assert */
        verify( mSession ).invalidate();
    }

    /**
     * <dt><b>Test Case UTC-44/CON-202</b></dt>
     *
     * Verify that given a valid response object, then the request response
     * should redirect to the index.jsp page.
     */
    @Test
    public final void doLogout_ValidResponseObject_RedirectsToIndexPage () throws Exception {
        /* Assert */
        verify( mResponse ).sendRedirect( "index.jsp" );
    }

}
