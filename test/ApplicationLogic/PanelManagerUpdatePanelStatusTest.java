package ApplicationLogic;

import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import Storage.DBManager;

/**
 * == PanelManager.updatePanelStatus Test Cases ==
 *
 * <dt><b>Note:</b></dt> Based on testing results for
 * DBManager.updatePanelStatus {@link DBManagerUpdatePanelStatusTest}, the
 * expected return values can be only true or false.
 *
 * <dt><b>Note:</b></dt> Per the specification of use case ISCUC-17 (Final
 * Document, page 17) the panel status can be  "ACTIVE, INACTIVE, IN PROGRESS,
 * COMPLETE, or any other..." Per this requirement it is assumed that
 * validation of input for the panelStatus request parameter is not needed with
 * exception of a blank string.
 *
 * <dt><b>Note:</b></dt> Per the specification of use case ISCUC-17 (Final
 * Document, page 17) an email notification is to be sent to all panelists if
 * the status is set to ACTIVE. Since Use Case ISCUC-27 (Send Email
 * Notification, Final Document, Page 119) this part of the specification will
 * not be tested.
 */

@PrepareForTest( { DBManager.class } )
public class PanelManagerUpdatePanelStatusTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private HttpServletRequest mRequest;
    private HttpServletResponse mResponse;

    private final String successPage = "messagePage";
    private final String successMessage = "?messageCode=Panel status updated.";
    private final String errorPage = "errorPage";
    private final String errorMessage = "?errorCode=Error updating panel. Please try again.";

    @Before
    public void setUp () throws Exception {
        PowerMockito.mockStatic( DBManager.class );

        mRequest = mock( HttpServletRequest.class );
        mResponse = mock( HttpServletResponse.class );
    }

    /**
     * <dt><b>Test Case UTC-33/PM-301</b></dt>
     *
     * Verify that given valid request parameters for panelStatus,
     * panelComments, and panelID, and the Storage subsystem does not encounter
     * errors, then the request response should redirect to the message page
     * with a success message.
     */
    @Test
    public final void updatePanelStatus_ValidRequestParams_RedirectsToSuccessMessagePage () throws Exception {
        /* Arrange */
        when( mRequest.getParameter( "panelStatus" ) ).thenReturn( "some-status" );
        when( mRequest.getParameter( "statusComments" ) ).thenReturn( "some comment" );
        when( mRequest.getParameter( "panelID" ) ).thenReturn( "42" );
        when( DBManager.updatePanelStatus( anyString(), anyString(), anyInt() ) ).thenReturn( true );

        /* Act */
        PanelManager.updatePanelStatus( mRequest, mResponse );

        /* Assert */
        verify( mResponse ).sendRedirect( successPage + successMessage );
    }

    /**
     * <dt><b>Test Case UTC-34/PM-302</b></dt>
     *
     * Verify that given the panelStatus request parameter is blank, the
     * panelComments is valid, and the panelID is valid, then the request
     * response should should redirect to the error page with an error
     * message.
     * 
     * <dt><b>Note:</b></dt> This test makes the assumption, based on testing from DBManager,
     *       that a call to DBManager.updatePanelStatus with a blank panelStatus
     *       string (first parameter) will return true. From this it is still
     *       expected, from a blackbox testing perspective, that the call to
     *       updatePanelStatus should handle it as an error.
     */
    @Test
    public final void updatePanelStatus_BlankPanelStatusParameter_RedirectsToErrorMessagePage () throws Exception {
        /* Arrange */
        when( mRequest.getParameter( "panelStatus" ) ).thenReturn( "" );
        when( mRequest.getParameter( "statusComments" ) ).thenReturn( "some comment" );
        when( mRequest.getParameter( "panelID" ) ).thenReturn( "42" );
        when( DBManager.updatePanelStatus( anyString(), anyString(), anyInt() ) ).thenReturn( true );

        /* Act */
        PanelManager.updatePanelStatus( mRequest, mResponse );

        /* Assert */
        verify( mResponse ).sendRedirect( errorPage + errorMessage );
    }

    /**
     * <dt><b>Test Case UTC-35/PM-303</b></dt>
     *
     * Verify that given the panelID is invalid and the Storage subsystem
     * returns false to indicate no panels with that id, then the request
     * response should redirect to the error page with an error message.
     */
    @Test
    public final void updatePanelStatus_InvalidPanelID_RedirectsToErrorMessagePage () throws Exception {
        /* Arrange */
        when( mRequest.getParameter( "panelStatus" ) ).thenReturn( "some panel status" );
        when( mRequest.getParameter( "statusComments" ) ).thenReturn( "some comment" );
        when( mRequest.getParameter( "panelID" ) ).thenReturn( "42" );
        when( DBManager.updatePanelStatus( anyString(), anyString(), anyInt() ) ).thenReturn( false );

        /* Act */
        PanelManager.updatePanelStatus( mRequest, mResponse );

        /* Assert */
        verify( mResponse ).sendRedirect( errorPage + errorMessage );
    }

    /**
     * <dt><b>Test Case UTC-36/PM-304</b></dt>
     *
     * Verify that given the panelID is blank, then the request response should
     * redirect to the error page with an error message.
     * 
     * <dt><b>Note:</b></dt> No specification could be found that the panel id
     * is required to be numeric so it no assumptions will be made that an
     * input panelID of only alphabet characters will cause/not cause an error.
     */
    @Test
    public final void updatePanelStatus_BlankPanelID_RedirectsToErrorMessagePage () throws Exception {
        /* Arrange */
        when( mRequest.getParameter( "panelStatus" ) ).thenReturn( "some panel status" );
        when( mRequest.getParameter( "statusComments" ) ).thenReturn( "some comment" );
        when( mRequest.getParameter( "panelID" ) ).thenReturn( "" );
        when( DBManager.updatePanelStatus( anyString(), anyString(), anyInt() ) ).thenReturn( false );

        /* Act */
        PanelManager.updatePanelStatus( mRequest, mResponse );

        /* Assert */
        verify( mResponse ).sendRedirect( errorPage + errorMessage );
    }

}
