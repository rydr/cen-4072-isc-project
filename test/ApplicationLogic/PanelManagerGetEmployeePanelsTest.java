package ApplicationLogic;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import Storage.DBManager;
import Storage.Repository.EmployeeProfile;
import Storage.Repository.Panel;

/**
 * == PanelManager.getEmployeePanels Test Cases ==
 *
 * <dt><b>Note:</b></dt> Based on testing results for DBManager.getPanels
 * {@link DBManagerGetPanelsTest}, the expected return values can be null, an
 * empty ArrayList, or an ArrayList of Panels.
 *
 * <dt><b>Note:</b></dt> It is assumed that any session passed in is a valid
 * session. We assume a correct implementation of the HttpSession interface is
 * implemented by the calling server, therefore we will not test for invalid
 * sessions.
 */

@PrepareForTest( { DBManager.class } )
public class PanelManagerGetEmployeePanelsTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private HttpSession mSession;
    private EmployeeProfile mProfile;

    /**
     * Returns a ArrayList collection of Panel objects in the size requested.
     * 
     * @param qty - the number of Panel objects to be in the collection
     * @return the ArrayList object
     */
    private ArrayList<Panel> getPanelFixtureCollection ( int qty ) {
        ArrayList<Panel> collection = new ArrayList<>();
        for ( int i = 0; i < qty; i++ ) {
            collection.add( new Panel() );
        }
        return collection;
    }

    @Before
    public void setUp () throws Exception {
        PowerMockito.mockStatic( DBManager.class );

        mSession = mock( HttpSession.class );
        mProfile = mock( EmployeeProfile.class );

        mProfile.EmployeeID = 42;
    }

    /**
     * <dt><b>Test Case UTC-37/PM-401</b></dt>
     *
     * Verify that given the employee id in the EmployeeProfile attached to the
     * session is valid and has Panels associated, then a non-empty ArrayList
     * of Panels will be returned.
     */
    @Test
    public final void getEmployeePanels_SessionEmployeeProfileHasPanels_NonEmptyArrayListOfPanel () throws Exception {
        /* Arrange */
        when( mSession.getAttribute( "User Profile" ) ).thenReturn( mProfile );
        when( DBManager.getPanels( anyInt() ) ).thenReturn( getPanelFixtureCollection( 5 ) );

        /* Act */
        ArrayList<Panel> result = PanelManager.getEmployeePanels( mSession );

        /* Assert */
        assertTrue( "result should be an ArrayList<Panel> of size greater than zer0", result.size() > 0 );
    }

    /**
     * <dt><b>Test Case UTC-38/PM-402</b></dt>
     *
     * Verify that given the employee id in the EmployeeProfile attached to the
     * session is valid and _does_not_ have Panel objects associated, then an
     * _empty_ ArrayList of Panel objects will be returned.
     */
    @Test
    public final void getEmployeePanels_SessionEmployeeProfileNoPanles_EmptyArrayListOfPanel () throws Exception {
        /* Arrange */
        when( mSession.getAttribute( "User Profile" ) ).thenReturn( mProfile );
        when( DBManager.getPanels( anyInt() ) ).thenReturn( getPanelFixtureCollection( 0 ) );

        /* Act */
        ArrayList<Panel> result = PanelManager.getEmployeePanels( mSession );

        /* Assert */
        assertEquals( 0, result.size() );
    }
}
