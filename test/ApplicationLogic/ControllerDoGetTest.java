package ApplicationLogic;

import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;

/**
 * == Controller.doPost Test Cases ==
 * 
 * <dt><b>Note:</b></dt> since the other methods have been unit tested and the
 * doGet method is an entry point to calling each of those methods, these tests
 * will only verify that the mapping is correct.
 */

public class ControllerDoGetTest {

    private Controller sController;
    private HttpServletRequest mRequest;
    private HttpServletResponse mResponse;
    private HttpSession mSession;

    @Before
    public void setUp () throws Exception {
        sController = spy( new Controller() );

        mRequest = mock( HttpServletRequest.class );
        mResponse = mock( HttpServletResponse.class );
        mSession = mock( HttpSession.class );

        when( mRequest.getSession( true ) ).thenReturn( mSession );
    }

    /**
     * <dt><b>Test Case UTC-57/CON-601</b></dt>
     *
     * Verify that given an "action-type" init param of "doLogout", then the
     * Controller.doLogout method should be called with the received response
     * object and a session object from the requesting session.
     */
    @Test
    public final void doGet_doLogoutInitParam_callsDoLogoutMethod () throws Exception {
        /* Arrange */
        doReturn( "doLogout" ).when( sController ).getInitParameter( "action-type" );

        /* Act */
        sController.doGet( mRequest, mResponse );

        /* Assert */
        verify( sController ).doLogout( mResponse, mSession );
    }

    /**
     * <dt><b>Test Case UTC-59/CON-602</b></dt>
     *
     * Verify that given an "action-type" init param of "root", then nothing
     * should be called.
     */
    @Test
    public final void doGet_InvalidInitParam_doesNothing () throws Exception {
        /* Arrange */
        doReturn( "root" ).when( sController ).getInitParameter( "action-type" );

        /* Act */
        sController.doPost( mRequest, mResponse );

        /* Assert */
        verify( sController ).doPost(  mRequest, mResponse );
        verify( sController ).getInitParameter( "action-type" );
        verifyNoMoreInteractions( sController );
    }

}
