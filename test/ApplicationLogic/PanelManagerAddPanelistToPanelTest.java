package ApplicationLogic;

import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import Storage.DBManager;

/**
 * == PanelManager.addPanelistToPanel Test Cases ==
 * 
 * <dt><b>Note:</b></dt> Based on testing results for
 * DBManager.addPanelistToPanel {@link DBManagerAddPanelistToPanelTest}, the
 * expected return values can be only true or false.
 */

@PrepareForTest( { DBManager.class } )
public class PanelManagerAddPanelistToPanelTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private HttpServletRequest mRequest;
    private HttpServletResponse mResponse;

    private final String successPage = "messagePage";
    private final String successMessage = "?messageCode=Panelist has been successfully added.";
    private final String errorPage = "errorPage";
    private final String errorMessage = "?errorCode=Error adding panelist. Please try again.";

    @Before
    public void setUp () throws Exception {
        PowerMockito.mockStatic( DBManager.class );

        mRequest = mock( HttpServletRequest.class );
        mResponse = mock( HttpServletResponse.class );

        when( mRequest.getParameter( "addToPanel" ) ).thenReturn( "42" );
        when( mRequest.getParameter( "panelistID" ) ).thenReturn( "42" );
    }

    /**
     * <dt><b>Test Case UTC-29/PM-201</b></dt>
     *
     * Verify that given the panel id and panelist id
     * passed in via the request parameters are valid and the database adds
     * successfully, then the request response will be to redirect to the
     * message page with a success message.
     */
    @Test
    public final void addPanelistToPanel_validPanelIDandPanelistID_RedirectsToSuccessMessagePage () throws Exception {
        /* Arrange */
        when( DBManager.addPanelistToPanel( anyInt(), anyInt() ) ).thenReturn( true );

        /* Act */
        PanelManager.addPanelistToPanel( mRequest, mResponse );

        /* Assert */
        verify( mResponse ).sendRedirect( successPage + successMessage );
    }

    /**
     * <dt><b>Test Case UTC-30/PM-202</b></dt>
     *
     * Verify that given the panel id and panelist id
     * combination contains invalid information and the Storage systems returns
     * false, then the request response should redirect to the error page with
     * an error message.
     */
    @Test
    public final void addPanelistToPanel_InvalidIDs_RedirectsToErrorMessagePage () throws Exception {
        /* Arrange */
        when( DBManager.addPanelistToPanel( anyInt(), anyInt() ) ).thenReturn( false );

        /* Act */
        PanelManager.addPanelistToPanel( mRequest, mResponse );

        /* Assert */
        verify( mResponse ).sendRedirect( errorPage + errorMessage );
    }

    /**
     * <dt><b>Test Case UTC-31/PM-203</b></dt>
     *
     * Verify that given a blank panel id and a valid panelist id passed in via
     * the request parameters, then the request should
     * not succeed.
     */
    @Test
    public final void addPanelistToPanel_MissingPanelID_RedirectsToErrorMessagePage () throws Exception {
        /* Arrange */
        when( mRequest.getParameter( "addToPanel" ) ).thenReturn( "" );

        /* Act */
        PanelManager.addPanelistToPanel( mRequest, mResponse );

        /* Assert */
        verify( mResponse ).sendRedirect( errorPage + errorMessage );
    }

    /**
     * <dt><b>Test Case UTC-32/PM-204</b></dt>
     *
     * Verify that given a valid panel id and a blank panelist id passed in via
     * the request parameters, then the request should not succeed.
     */
    @Test
    public final void addPanelistToPanel_MissingPanelistID_RedirectsToErrorMessagePage () throws Exception {
        /* Arrange */
        when( mRequest.getParameter( "panelistID" ) ).thenReturn( "" );

        /* Act */
        PanelManager.addPanelistToPanel( mRequest, mResponse );

        /* Assert */
        verify( mResponse ).sendRedirect( errorPage + errorMessage );
    }

}
