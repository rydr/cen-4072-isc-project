package ApplicationLogic;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import Interface.UserForms;
import Storage.DBManager;
import Storage.Repository.EmployeeProfile;
import Storage.Repository.UserProfile;
import Storage.Repository.UserType;

/**
 * == Controller.doLogin Test Cases ==
 * 
 * <ul>
 * <li> User Manual, Page 5, Getting Started: ISC Login</li>
 * <li> Final Document, Page 43, Login Sequence Diagram</li>
 * <li> Final Document, Page 62, Use Case ISCUC-03 - Login</li>
 * </ul>
 * 
 * <dt><b>Note:</b></dt> Based on testing results for
 * DBManager.validateUsername {@link DBManagerValidateUsernameTest}, the
 * expected return values can be one of Null, a UserProfile object, or an
 * EmployeeProfile object.
 * 
 * <dt><b>Note:</b></dt> Based on testing of the DBManager.validateUsername
 * {@link DBManagerValidateUsernameTest}, passing blank username or password
 * strings should still be valid but will more than likely fail validation.
 * Thus, we will not test validation of blank input in the request object.
 */

@PrepareForTest( { DBManager.class } )
public class ControllerDoLoginTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private HttpServletRequest mRequest;
    private HttpServletResponse mResponse;
    private HttpSession mSession;
    private UserProfile mProfile;

    @Before
    public void setUp () throws Exception {
        PowerMockito.mockStatic( DBManager.class );

        mRequest = mock( HttpServletRequest.class );
        mResponse = mock( HttpServletResponse.class );
        mSession = mock( HttpSession.class );
        mProfile = mock( UserProfile.class );

        mProfile.Type = UserType.PANELIST;
    }

    /**
     * <dt><b>Test Case UTC-39/CON-101</b></dt>
     *
     * Verify that given valid request, response, and session objects and the
     * username and password request parameters are valid, then the supplied
     * session should contain a UserProfile object for
     * the user.
     */
    @Test
    public final void doLogin_ValidUserCredentials_SetsUserProfileInSession () throws Exception {
        /* Arrange */
        ArgumentCaptor<UserProfile> profileArg = ArgumentCaptor.forClass( UserProfile.class );
        when( DBManager.validateUsername( anyString(), anyString() ) ).thenReturn( mProfile );

        /* Act */
        new Controller().doLogin( mRequest, mResponse, mSession );

        /* Assert */
        verify( mSession ).setAttribute( eq( "User Profile" ), profileArg.capture() );
        assertSame( mProfile, profileArg.getValue() );
    }

    /**
     * <dt><b>Test Case UTC-40/CON-102</b></dt>
     *
     * Verify that given valid request, response, and session objects and the
     * username and password request parameters are valid for an Employee, then
     * the supplied session should contain an EmployeeProfile object for the
     * user.
     */
    @Test
    public final void doLogin_ValidEmployeeCredentials_SetsEmployeeProfileInSession () throws Exception {
        /* Arrange */
        ArgumentCaptor<EmployeeProfile> profileArg = ArgumentCaptor.forClass( EmployeeProfile.class );

        mProfile = mock( EmployeeProfile.class );
        mProfile.Type = UserType.EMPLOYEE;
        when( DBManager.validateUsername( anyString(), anyString() ) ).thenReturn( mProfile );

        /* Act */
        new Controller().doLogin( mRequest, mResponse, mSession );

        /* Assert */
        verify( mSession ).setAttribute( eq( "User Profile" ), profileArg.capture() );
        assertSame( mProfile, profileArg.getValue() );
    }

    /**
     * <dt><b>Test Case UTC-41/CON-103</b></dt>
     *
     * Verify that given valid request, response, and session objects and the
     * username and password request parameters are an invalid combination,
     * then the response request should redirect to the login page.
     */
    @Test
    public final void doLogin_InvalidUserCredentials_RedirectsToLoginPage () throws Exception {
        /* Arrange */
        when( DBManager.validateUsername( anyString(), anyString() ) ).thenReturn( null );

        /* Act */
        new Controller().doLogin( mRequest, mResponse, mSession );

        /* Assert */
        verify( mResponse ).sendRedirect( "login" );
        verifyZeroInteractions( mSession );
    }

    /**
     * <dt><b>Test Case UTC-42/CON-104</b></dt>
     *
     * Verify that given valid request, response, an session objects and the
     * username and password request parameters are a valid combination, then a
     * UserForm for the user profile type will be set in the "User Form" key in
     * the supplied session.
     * 
     * <dt><b>Reference</b></dt>Log In Sequence Diagram (referenced in class
     * header comments)
     */
    @Test
    public final void doLogin_ValidUserCredentials_SetsUserFormSessionKey () throws Exception {
        for ( UserType type : UserType.values() ) {
            /* Ignore INVALID user type, assume valid user profiles */
            if ( type == UserType.INVALID ) continue;

            /* Arrange */
            ArgumentCaptor<UserForms> formArg = ArgumentCaptor.forClass( UserForms.class );

            mProfile.Type = type;
            when( DBManager.validateUsername( anyString(), anyString() ) ).thenReturn( mProfile );

            mSession = mock( HttpSession.class );

            /* Act */
            new Controller().doLogin( mRequest, mResponse, mSession );

            /* Assert */
            verify( mSession ).setAttribute( eq( "User Form" ), formArg.capture() );
            assertTrue( type.name() + " should be an instance of UserForms", formArg.getValue() instanceof UserForms );
        }
    }
}
