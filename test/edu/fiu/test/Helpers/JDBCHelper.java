package edu.fiu.test.Helpers;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.powermock.api.mockito.PowerMockito;

import Storage.Repository.DBConnection;

public class JDBCHelper {

    /**
     * Mocks the DBConnection class interactions and returns a mocked JDBC
     * Connection class.
     */
    public static Connection mockDBConnection() throws Exception {
        Connection mConnection = mock( Connection.class );
        DBConnection dbc = mock( DBConnection.class );
        PowerMockito.mockStatic( DBConnection.class );

        when( DBConnection.instance() ).thenReturn( dbc );
        when( dbc.getConnection() ).thenReturn( mConnection );

        return mConnection;
    }

    /**
     * Returns a mock Statement object attached to the supplied Connection.
     */
    public static Statement mockDBStatement( Connection connection ) throws Exception {
        Statement mStatement = mock( Statement.class );

        when( connection.createStatement() ).thenReturn( mStatement );

        return mStatement;
    }

    /**
     * Mocks SELECT count(*) queries to the supplied mock Statement object. This
     * method implicitly mocks the behavior of ResultSet.next() to return true.
     * 
     * @TODO Look into how to use Mockito.matches(...) for regular expression
     *       matching of arguments
     */
    public static ResultSet mockSelectCountQuery( String table, int count, Statement mockStatement ) throws Exception {
        ResultSet mResultSet = mock( ResultSet.class );

        when( mockStatement.executeQuery( contains( "SELECT count(*) FROM " + table ) ) ).thenReturn( mResultSet );
        when( mResultSet.next() ).thenReturn( true );
        when( mResultSet.getInt( anyString() ) ).thenReturn( count );

        return mResultSet;
    }

    /**
     * Mocks SELEC MAX queries to the supplied mock Statement object. This
     * method implicitly mocks the behavior of ResultSet.next() to return true.
     * 
     * @TODO Look into how to use Mockito.matches(...) for regular expression
     *       matching of arguments
     */
    public static ResultSet mockSelectMaxQuery( String table, String col, int max, Statement mStatement )
            throws Exception {
        String query = "SELECT MAX(" + col + ") FROM " + table;
        ResultSet mResultSet = mock( ResultSet.class );

        when( mStatement.executeQuery( contains( query ) ) ).thenReturn( mResultSet );
        when( mResultSet.next() ).thenReturn( true );
        when( mResultSet.getInt( anyString() ) ).thenReturn( max );

        return mResultSet;
    }

}
