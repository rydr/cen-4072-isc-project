package edu.fiu.test.Fixtures;

import Storage.Repository.EmployeeProfile;
import Storage.Repository.UserProfile;
import Storage.Repository.UserType;

public class EmployeeProfileFixture {

    public static final EmployeeProfile defaultProfile () {
        EmployeeProfile defaults = new EmployeeProfile( new UserProfile() );

        defaults.UserID = 100;
        defaults.UserName = "donuteatme";
        defaults.FirstName = "Homer";
        defaults.LastName = "Simpson";
        defaults.Locked = 0;
        defaults.Type = UserType.EMPLOYEE;
        defaults.EmployeeID = 9001;
        defaults.TypeOfEmployee = 0;

        return defaults;
    }

}
