package edu.fiu.test.Fixtures;

import Storage.Repository.UserProfile;
import Storage.Repository.UserType;

public class UserProfileFixture {

    public static final UserProfile defaultUserProfile() {
        UserProfile defaults = new UserProfile();

        defaults.UserID = 100;
        defaults.UserName = "thepanelist";
        defaults.FirstName = "Lady";
        defaults.LastName = "Fixture";
        defaults.Locked = 0;
        defaults.Type = UserType.PANELIST;

        return defaults;
    }

}
