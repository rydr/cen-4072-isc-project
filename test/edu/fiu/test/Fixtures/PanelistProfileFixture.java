package edu.fiu.test.Fixtures;

import java.util.Collection;
import java.util.HashMap;

import Storage.Repository.PanelistProfile;
import Storage.Repository.UserType;

public class PanelistProfileFixture {
    private static final HashMap<String, String> defaults = new HashMap<>();

    static {
        defaults.put( "username", "theclarkester" );
        defaults.put( "password", "testing123" );
        defaults.put( "pISCID", "1001" );
        defaults.put( "pFName", "Peter" );
        defaults.put( "pLName", "Clarkester" );
        defaults.put( "pInstitution", "fiu" );
        defaults.put( "pAddress", "123 FIU Lane" );
        defaults.put( "pCity", "Miami" );
        defaults.put( "pState", "FL" );
        defaults.put( "pZip", "33172" );
        defaults.put( "pTelephone", "305-867-5309" );
        defaults.put( "pEmail", "clarkester@fiu.edu" );
        defaults.put( "pGender", "Male" );
        defaults.put( "pEthnicity", "Caribbean" );
        defaults.put( "pExpertise", "Testing" );
    }

    /**
     * HashMap structure used for creation of Panelist accounts
     * {@link ApplicationLogic.RegistrationManager}
     */
    @SuppressWarnings( "unchecked" )
    public static HashMap<String, String> defaultCreationHash() {
        return (HashMap<String, String>) defaults.clone();
    }

    /**
     * Default PanelistProfile Test Fixture
     * {@link Storage.Respository.PanelistProfile}
     */
    public static PanelistProfile defaultProfile() {
        PanelistProfile panelist = new PanelistProfile();

        panelist.UserName = defaults.get( "username" );
        panelist.ISCID = defaults.get( "pISCID" );
        panelist.FirstName = defaults.get( "pFName" );
        panelist.LastName = defaults.get( "pLName" );
        panelist.Institution = defaults.get( "pInstitution" );
        panelist.Address = defaults.get( "pAddress" );
        panelist.City = defaults.get( "pCity" );
        panelist.State = defaults.get( "pState" );
        panelist.ZipCode = defaults.get( "pZip" );
        panelist.Telephone = defaults.get( "pTelephone" );
        panelist.Email = defaults.get( "pEmail" );
        panelist.Gender = defaults.get( "pGender" );
        panelist.Ethnicity = defaults.get( "pEthnicity" );
        panelist.Expertise = defaults.get( "pExpertise" );
        panelist.Locked = 0;
        panelist.Type = UserType.PANELIST;
        panelist.UserID = 123;

        return panelist;
    }

    /**
     * Populate a collection with default PanelistProfile objects.
     * @see PanelistProfileFixture.defaultProfile() Method
     */
    public static void defaultProfileCollection( int qty, Collection<PanelistProfile> collection ) {
        for ( int cnt = 0; cnt < qty; cnt++ ) {
            collection.add( defaultProfile() );
        }
    }
}
