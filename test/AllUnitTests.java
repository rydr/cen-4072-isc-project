import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith( Suite.class )
@SuiteClasses( { ControllerTestSuite.class, PanelManagerTestSuite.class, DBManagerTestSuite.class } )
public class AllUnitTests {

}
