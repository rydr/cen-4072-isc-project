import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ApplicationLogic.PanelManagerAddPanelistToPanelTest;
import ApplicationLogic.PanelManagerCreatePanelTest;
import ApplicationLogic.PanelManagerGetEmployeePanelsTest;
import ApplicationLogic.PanelManagerUpdatePanelStatusTest;

/**
 * @note The .getPanel and .isPanelistRegistered methods of the PanelManager
 *       class are not tested because they do not have implementation to test.
 * 
 */

@RunWith( Suite.class )
@SuiteClasses({
    PanelManagerCreatePanelTest.class, PanelManagerAddPanelistToPanelTest.class,
    PanelManagerUpdatePanelStatusTest.class, PanelManagerGetEmployeePanelsTest.class
})
public class PanelManagerTestSuite {

}
