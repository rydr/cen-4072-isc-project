package Storage;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import Storage.Repository.DBConnection;
import edu.fiu.test.Helpers.JDBCHelper;

/**
 * == DBManager.addPanelistToPanel Test Cases ==
 *
 * Test cases based on requirements and specifications described in the ISC
 * documentation.
 *
 * <ul>
 * <li> User Manual, Page 14, Add Panelist To Panel</li>
 * <li> Final Document, Page 15, Add Panelist To Panel Function Requirement</li>
 * <li> Final Document, Page 22, Add Panelist to Panel Nonfunction Requirement</li>
 * <li> Final Document, Page 47, Add Panelist to Panel Sequence Diagram</li>
 * <li> Final Document, Page 52, PanelContainer Storage Class Description</li>
 * <li> Final Document, Page 82, Use Case ISCUC-11 Add Panelist to Panel</li>
 * </ul>
 *
 * <dt><b>Note:</b></dt> Assumes {@link DBConnection} connects with a valid
 * connection _therefore_ tests will use a mocked connection.
 *
 * @todo create a test for negative input integers, e.g. negative panelistID
 */

@PrepareForTest( { DBConnection.class, JDBCHelper.class } )
public class DBManagerAddPanelistToPanelTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private Connection mConnection;
    private Statement mStatement;
    private int panelistID;
    private int panelID;

    private final ResultSet mockSelectCountQuery ( int count, Statement mStatement ) throws Exception {
        ResultSet mResultSet = mock( ResultSet.class );
        String catch_query = "SELECT count(.*) FROM servingpanelists WHERE panelistid=.*AND panelID=.*";

        when( mStatement.executeQuery( matches( catch_query ) ) ).thenReturn( mResultSet );
        when( mResultSet.getInt( "count(*)" ) ).thenReturn( count );

        return mResultSet;
    }

    @Before
    public void setUp () throws Exception {
        mConnection = JDBCHelper.mockDBConnection();
        mStatement = JDBCHelper.mockDBStatement( mConnection );
        panelistID = panelID = 42;
    }

    /**
     * <dt><b>Test Case UTC-18/DBM-501</b></dt>
     *
     * Verifies that given a panelist is already on a
     * panel, then false will be returned to the caller.
     */
    @Test
    public final void addPanelistToPanel_PanelistAlreadyOnPanel_ReturnsFalse () throws Exception {
        /* Arrange */
        mockSelectCountQuery( 1, mStatement );

        /* Act */
        boolean result = DBManager.addPanelistToPanel( panelistID, panelID );

        /* Assert */
        assertFalse( "should return false when panelist already in panel", result );
    }

    /**
     * <dt><b>Test Case UTC-19/DBM-502</b></dt>
     *
     * Verifies that given a panelist is not already on a
     * panel, then true will be returned to the caller when the panelist is
     * added.
     */
    @Test
    public final void addPanelistToPanel_PanelistNotOnPanel_ReturnsTrue () throws Exception {
        /* Arrange */
        mockSelectCountQuery( 0, mStatement );

        /* Act */
        boolean result = DBManager.addPanelistToPanel( panelistID, panelID );

        /* Assert */
        assertTrue( "should return true when panelist is added", result );
    }

    /**
     * <dt><b>Test Case UTC-20/DBM-503</b></dt>
     *
     * Verifies that when an error occurs at the database
     * level and a SQLException is thrown, the exception will be handled and
     * false will be returned to the caller.
     */
    @SuppressWarnings( "unchecked" )
    @Test
    public final void addPanelistToPanel_SQLExceptionThrown_ReturnsFalse () throws Exception {
        /* Arrange */
        mockSelectCountQuery( 0, mStatement );
        when( mStatement.executeUpdate( anyString() ) ).thenThrow( SQLException.class );

        /* Act */
        boolean result = DBManager.addPanelistToPanel( panelistID, panelID );

        /* Assert */
        assertFalse( "should return false when an SQLException is thrown", result );
    }

}
