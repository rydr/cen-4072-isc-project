package Storage;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import edu.fiu.test.Helpers.JDBCHelper;

import Storage.Repository.DBConnection;
import Storage.Repository.Panel;

/**
 * == DBManager.getPanels Test Cases ==
 *
 * Test cases based on requirements and specifications described in the ISC
 * documentation.
 *
 * <ul>
 * <li> User Manual, Page 16, View Panel Information</li>
 * <li> Final Document, Page 16, View Panel Information Functional Requirements</li>
 * <li> Final Document, Page 24, View Panel Info NonFunctional Requirements</li>
 * <li> Final Document, Page 37, Panels Persistent Data Management</li>
 * <li> Final Document, Page 45, View Panels Info Sequence Diagram</li>
 * <li> Final Document, Page 52, PanelContainer Class Description</li>
 * <li> Final Document, Page 52, Panel Class Description</li>
 * <li> Final Document, Page 103, Use Case ISCUC-19 View Panel Information</li>
 * </ul>
 *
 * <dt><b>Note:</b></dt> Assumes: {@link DBConnection} connects with a valid connection
 *       _therefore_ tests will use a mocked connection.
 *
 * @todo create a test for negative integer on employeeid
 */

@PrepareForTest( { DBConnection.class } )
public class DBManagerGetPanelsTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private Connection mConnection;
    private Statement mStatement;
    private int employeeID;

    /**
     * Intercept select queries for panels which are accessible by an employee
     * id and mock the interactions to provide a mocked ResultSet.
     *
     * @param eid - the employee id
     * @param hasPanels - controls if the Panels Query ResultSet is empty or not
     * @param mStatement - the Connection Statement to bind the mocked ResultSet
     *            to
     * @return the ResultSet created
     */
    private ResultSet mockSelectEmployeePanels ( int eid, boolean hasPanels, Statement mStatement ) throws Exception {
        ResultSet mResultSet = mock( ResultSet.class );
        String catch_query = "SELECT t1.PanelID, Name, Description FROM panelaccess as t1, panel as t2";

        when( mStatement.executeQuery( contains( catch_query ) ) ).thenReturn( mResultSet );
        when( mResultSet.next() ).thenReturn( hasPanels );

        return mResultSet;
    }

    @Before
    public void setUp () throws Exception {
        mConnection = JDBCHelper.mockDBConnection();
        mStatement = JDBCHelper.mockDBStatement( mConnection );
        employeeID = 42;
    }

    /**
     * <dt><b>Test Case UTC-23/DBM-701</b></dt>
     *
     * Verifies that given an employee id, when the
     * employee does not have access to any panels, then an empty ArrayList
     * should be returned.
     */
    @Test
    public final void getPanels_EmployeeIDHasNoPanels_ReturnsEmptyArrayList () throws Exception {
        /* Arrange */
        mockSelectEmployeePanels( employeeID, false, mStatement );

        /* Act */
        ArrayList<Panel> result = DBManager.getPanels( employeeID );

        /* Assert */
        assertEquals( 0, result.size() );
    }

    /**
     * <dt><b>Test Case UTC-24/DBM-702</b></dt>
     *
     * Verifies that an error occurring at the database
     * level which throws a SQLException will be handled and thus returning a
     * null object;
     */
    @SuppressWarnings( "unchecked" )
    @Test
    public final void getPanels_DBErrorThrowsSQLException_ReturnsNullReference () throws Exception {
        /* Arrange */
        when( mStatement.executeQuery( anyString() ) ).thenThrow( SQLException.class );

        /* Act */
        ArrayList<Panel> result = DBManager.getPanels( employeeID );

        /* Assert */
        assertNull( "should return null when SQLException occurs", result );
    }

    /**
     * <dt><b>Note:</b></dt> The construction of the found panels will not be
     * tested since it would normally be encapsulated in an DAO that returns
     * the converted collection. Since input in the DBManager API only takes an
     * Employee ID we can only assume that the behavior would be it can find or
     * not find panels. Mocking the interactions with the database that
     * performs the conversion does prove that the unit under test works or
     * doesn't work when isolated from external systems, i.e. the database
     * system. The construction process will be left for integration testing
     * with external systems.
     */

}
