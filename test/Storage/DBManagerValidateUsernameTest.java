package Storage;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import edu.fiu.test.Fixtures.EmployeeProfileFixture;
import edu.fiu.test.Fixtures.UserProfileFixture;
import edu.fiu.test.Helpers.JDBCHelper;

import Storage.Repository.DBConnection;
import Storage.Repository.EmployeeProfile;
import Storage.Repository.UserProfile;

/**
 * == DBManager.validateUsername Test Cases ==
 * 
 * Test cases based on requirements and specifications described in the ISC
 * documentation.
 * 
 * <dt><b>References:</b></dt>
 * <ul>
 * <li>User Manual, Page 5, Getting Started: ISC Login</li>
 * <li>Final Document, Page 41, AccountManager class description</li>
 * <li>Final Document, Page 43, Login Sequence Diagram</li>
 * <li>Final Document, Page 52, AccountManager Detailed Class Design</li>
 * <li>Final Document, Page 62, Use Case ISCUC-03 - Login</li>
 * </ul>
 * 
 * <dt><b>Note:</b></dt> Username and password inputs are only used in an
 * interaction with the database. While we do input a username and password into
 * the method under test, we chose to mock that interaction by providing a
 * ResultSet that will be either true or false when called with next(). The
 * results being asserted are to verify what the unit under test does with that
 * result.
 * 
 * <dt><b>Note:</b></dt> Assumes {@link DBConnection} connects with a valid
 * connection _therefore_ tests will use a mocked connection.
 */

@PrepareForTest( { DBConnection.class, JDBCHelper.class } )
public class DBManagerValidateUsernameTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private Connection mConnection;
    private Statement mStatement;
    private String username;
    private String password;

    /**
     * Mock a database query searching for a username AND password in the
     * iscusers table.
     * 
     * @param isValid whether the username and password combination will be
     *            valid
     * @param id the id to return if true
     * @param mStatement the statement object performing the query
     * @return the mock ResultSet object returned by the query
     */
    private final ResultSet mockCredentialQuery ( boolean isValid, int id, Statement mStatement ) throws Exception {
        ResultSet mResultSet = mock( ResultSet.class );
        String catch_query = "SELECT id FROM iscusers WHERE username=.*AND password=.*";

        when( mStatement.executeQuery( matches( catch_query ) ) ).thenReturn( mResultSet );
        when( mResultSet.next() ).thenReturn( isValid );
        when( mResultSet.getInt( "id" ) ).thenReturn( id );

        return mResultSet;
    }

    /**
     * Intercept queries for iscusers where id is equal to the id found in the
     * username and password query. Mock the interactions to return a ResultSet
     * using the supplied UserProfile.
     * 
     * @param profile - the UserProfile to return in the mocked ResultSet
     * @param mStatement - the mock Statement to bind the mock ResultSet to
     * @return the mocked ResultSet
     */
    private final ResultSet mockISCUsersQuery ( UserProfile profile, Statement mStatement ) throws Exception {
        ResultSet mResultSet = mock( ResultSet.class );
        String catch_query = "SELECT * FROM iscusers WHERE id=";
        boolean hasNext = ( profile != null );

        when( mStatement.executeQuery( contains( catch_query ) ) ).thenReturn( mResultSet );
        when( mResultSet.next() ).thenReturn( hasNext );

        when( mResultSet.getInt( "ID" ) ).thenReturn( profile.UserID );
        when( mResultSet.getString( "Username" ) ).thenReturn( profile.UserName );
        when( mResultSet.getString( "FirstName" ) ).thenReturn( profile.FirstName );
        when( mResultSet.getString( "LastName" ) ).thenReturn( profile.LastName );
        when( mResultSet.getInt( "Locked" ) ).thenReturn( profile.Locked );
        when( mResultSet.getInt( "UserType" ) ).thenReturn( profile.Type.ordinal() );

        return mResultSet;
    }

    /**
     * Intercept queries for iscemployees where the userid is equal to the id
     * found in the username and password query. Mock the interections to return
     * a ResultSet using the supplied EmployeeProfile.
     * 
     * @param profile - the EmployeeProfile to return in the mocked ResultSet
     * @param mStatement - the mock Statement to bind the mock ResultSet to
     * @return the mocked ResultSet
     */
    private final ResultSet mockEmployeeQuery ( EmployeeProfile profile, Statement mStatement ) throws Exception {
        ResultSet mResultSet = mock( ResultSet.class );
        String catch_query = "SELECT * FROM iscemployee WHERE userid=";

        when( mStatement.executeQuery( contains( catch_query ) ) ).thenReturn( mResultSet );
        when( mResultSet.getInt( "EID" ) ).thenReturn( profile.EmployeeID );
        when( mResultSet.getInt( "Type" ) ).thenReturn( profile.TypeOfEmployee );

        return mResultSet;
    }

    /**
     * Helper method to compare two UserProfile objects.
     */
    private final boolean userProfilesEqual ( UserProfile expected, UserProfile received ) {
        if ( expected.UserID != received.UserID ) return false;
        if ( expected.UserName != received.UserName ) return false;
        if ( expected.FirstName != received.FirstName ) return false;
        if ( expected.LastName != received.LastName ) return false;
        if ( expected.Locked != received.Locked ) return false;
        if ( expected.Type != received.Type ) return false;
        return true;
    }

    @Before
    public void setUp () throws Exception {
        mConnection = JDBCHelper.mockDBConnection();
        mStatement = JDBCHelper.mockDBStatement( mConnection );
        username = "some-username";
        password = "some-password";
    }

    /**
     * <dt><b>Test Case UTC-12/DBM-301</b></dt>
     *
     * Verify _both_ username and password not in the database does _not_
     * return a user profile.
     */
    @Test
    public final void validateUsername_bothCredentialsNotInDB_returnsNull () throws Exception {
        /* Arrange */
        mockCredentialQuery( false, 0, mStatement );

        /* Act */
        UserProfile result = DBManager.validateUsername( username, password );

        /* Assert */
        assertNull( result );
    }

    /**
     * <dt><b>Test Case UTC-13/DBM-302</b></dt>
     *
     * Verify that a UserProfile object is returned when the database returns
     * an id for the username and password combination.
     */
    @Test
    public final void validateUsername_credentialsFoundInDB_returnsUserProfile () throws Exception {
        /* Arrange */
        UserProfile profile = UserProfileFixture.defaultUserProfile();

        mockCredentialQuery( true, profile.UserID, mStatement );
        mockISCUsersQuery( profile, mStatement );

        /* Act */
        UserProfile result = DBManager.validateUsername( username, password );

        /* Assert */
        assertTrue( "returned userprofile should be equal to mocked database result",
                userProfilesEqual( profile, result ) );
    }

    /**
     * <dt><b>Test Case UTC-14/DBM-303</b></dt>
     *
     * Verify that an EmployeeProfile object is returned if the id returned by
     * the username and password query is associated with an Employee user
     * type.
     */
    @Test
    public final void validateUsername_userIsAnEmployee_returnsEmployeeProfile () throws Exception {
        /* Arrange */
        EmployeeProfile profile = EmployeeProfileFixture.defaultProfile();

        mockCredentialQuery( true, profile.UserID, mStatement );
        mockISCUsersQuery( profile, mStatement );
        mockEmployeeQuery( profile, mStatement );

        /* Act */
        UserProfile result = DBManager.validateUsername( username, password );

        /* Assert */
        assertTrue( "returned userprofile should be equal to mocked database result",
                userProfilesEqual( profile, result ) );
    }

    /**
     * <dt><b>Test Case UTC-15/DBM-305</b></dt>
     *
     * Verify that a SQLException is handled and null is returned.
     */
    @Test
    @SuppressWarnings( "unchecked" )
    public final void validateUsername_SQLExceptionThrown_returnsNull () throws Exception {
        /* Arrange */
        ResultSet mResultSet = mockCredentialQuery( true, 0, mStatement );
        when( mResultSet.next() ).thenThrow( SQLException.class );

        /* Act */
        UserProfile result = DBManager.validateUsername( username, password );

        /* Assert */
        assertNull( result );
    }
}
