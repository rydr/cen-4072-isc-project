package Storage;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import Storage.Repository.DBConnection;
import Storage.Repository.PanelistContainer;
import Storage.Repository.PanelistProfile;

import edu.fiu.test.Fixtures.PanelistProfileFixture;
import edu.fiu.test.Helpers.JDBCHelper;

/**
 * == DBManager.getPanelists Test Cases ==
 * 
 * <dt><b>References:</b></dt>
 * <ul>
 * <li>User Guide, Page 13,</li>
 * <li>Final Document, Page 15, Search Panelist in Database</li>
 * <li>Final Document, Page 53, DBManager Class Description</li>
 * <li>Final Document, Page 53, PanelistContainer Class Description</li>
 * <li>Final Document, Page 79, Use Case ISCUC-10 Search Panelist in Database</li>
 * </ul>
 * 
 * <dt><b>Note:</b></dt> Assumes {@link DBConnection} connects with a valid
 * connection _therefore_ tests will use a mocked connection.
 * 
 */

@PrepareForTest( { DBConnection.class, PanelistContainer.class } )
public class DBManagerGetPanelistTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private HashMap<String, String> criteria;
    private ArrayList<PanelistProfile> datapool;

    @Before
    public void setUp () throws Exception {
        criteria = new HashMap<>();
        datapool = new ArrayList<>();

        PowerMockito.mockStatic( PanelistContainer.class );
    }

    /**
     * <dt><b>Test Case UTC-05/DBM-201</b></dt> Verify a single valid criteria
     * value will return the correct result in a pool with no other matching
     * criteria.
     * 
     * <dt><b>Note:</b></dt> No requirement or specification found as to how the
     * search should behave. An assumption is made that if a single criteria is
     * requested from a database which has a single matching value then it
     * should be processed and returned.
     */
    @Test
    public final void getPanelists_singleCriteria_returnsOneResult () throws Exception {
        /* Arrange */
        criteria.put( "ISCID", "1001" );

        PanelistProfileFixture.defaultProfileCollection( 3, datapool );
        datapool.get( 0 ).ISCID = "1001"; // should be found
        datapool.get( 1 ).ISCID = "2000";
        datapool.get( 2 ).ISCID = "2001";

        when( PanelistContainer.getPanelists( criteria ) ).thenCallRealMethod();
        when( PanelistContainer.getAllPanelists() ).thenReturn( datapool );

        /* Act */
        ArrayList<PanelistProfile> result = DBManager.getPanelists( criteria );

        /* Assert */
        assertEquals( datapool.get( 0 ), result.get( 0 ) );
    }

    /**
     * <dt><b>Test Case UTC-06/DBM-202</b></dt>
     * 
     * Verify a single valid criteria will return multiple matching results.
     */
    @Test
    public final void getPanelist_singleCriteria_returnsMultipleResults () throws Exception {
        /* Arrange */
        criteria.put( "Institution", "FIU" );

        PanelistProfileFixture.defaultProfileCollection( 3, datapool );
        datapool.get( 0 ).Institution = "Carnegie Mellon University";
        datapool.get( 1 ).Institution = "FIU";
        datapool.get( 2 ).Institution = "FIU";

        when( PanelistContainer.getPanelists( criteria ) ).thenCallRealMethod();
        when( PanelistContainer.getAllPanelists() ).thenReturn( datapool );

        /* Act */
        ArrayList<PanelistProfile> result = DBManager.getPanelists( criteria );

        /* Assert */
        assertEquals( 2, result.size() );
        assertEquals( criteria.get( "Institution" ), result.get( 0 ).Institution );
        assertEquals( criteria.get( "Institution" ), result.get( 1 ).Institution );
    }

    /**
     * <dt><b>Test Case UTC-07/DBM-203</b></dt>
     * 
     * Verify multiple search criteria will return a single matching result.
     * 
     * <dt><b>Note:</b></dt> Assumption is made that a match means _all_
     * criteria was met.
     */
    @Test
    public final void getPanelist_multipleCriteria_returnsSingleResult () throws Exception {
        /* Arrange */
        criteria.put( "Institution", "FIU" );
        criteria.put( "Expertise", "Testing" );

        PanelistProfileFixture.defaultProfileCollection( 3, datapool );
        datapool.get( 0 ).Institution = "Carnegie Mellon University";
        datapool.get( 0 ).Expertise = "Testing";
        datapool.get( 1 ).Institution = "FIU";
        datapool.get( 1 ).Expertise = "Sleeping";
        // should be match
        datapool.get( 2 ).Institution = "FIU";
        datapool.get( 2 ).Expertise = "Testing";

        when( PanelistContainer.getPanelists( criteria ) ).thenCallRealMethod();
        when( PanelistContainer.getAllPanelists() ).thenReturn( datapool );

        /* Act */
        ArrayList<PanelistProfile> result = DBManager.getPanelists( criteria );

        /* Assert */
        assertEquals( 1, result.size() );
        assertEquals( criteria.get( "Institution" ), result.get( 0 ).Institution );
        assertEquals( criteria.get( "Expertise" ), result.get( 0 ).Expertise );
    }

    /**
     * <dt><b>Test Case UTC-08/DBM-204</b></dt>
     * 
     * Verify multiple search criteria will return multiple matching results.
     */
    @Test
    public final void getPanelist_multipleCriteria_returnsMultipleResults () throws Exception {
        /* Arrange */
        criteria.put( "FirstName", "Peter" );
        criteria.put( "Expertise", "Testing" );

        PanelistProfileFixture.defaultProfileCollection( 4, datapool );
        datapool.get( 0 ).FirstName = "Jerry";
        datapool.get( 0 ).Expertise = "Testing";
        datapool.get( 1 ).FirstName = "Peter";
        datapool.get( 1 ).Expertise = "Comedy";

        when( PanelistContainer.getPanelists( criteria ) ).thenCallRealMethod();
        when( PanelistContainer.getAllPanelists() ).thenReturn( datapool );

        /* Act */
        ArrayList<PanelistProfile> result = DBManager.getPanelists( criteria );

        /* Assert */
        assertEquals( 2, result.size() );
        assertEquals( criteria.get( "FirstName" ), result.get( 0 ).FirstName );
        assertEquals( criteria.get( "Expertise" ), result.get( 0 ).Expertise );
    }

    /**
     * <dt><b>Test Case UTC-09/DBM-205</b></dt>
     * 
     * Verify an empty Collection is returned if nothing matches the search
     * criteria.
     */
    @Test
    public final void getPanelist_noMatchingCriteria_returnsNoResults () {
        /* Arrange */
        criteria.put( "FirstName", "Mclovin" ); // remember the default is Peter

        PanelistProfileFixture.defaultProfileCollection( 10, datapool );

        when( PanelistContainer.getPanelists( criteria ) ).thenCallRealMethod();
        when( PanelistContainer.getAllPanelists() ).thenReturn( datapool );

        /* Act */
        ArrayList<PanelistProfile> result = DBManager.getPanelists( criteria );

        /* Assert */
        assertEquals( 0, result.size() );
    }

    /**
     * <dt><b>Test Case UTC-10/DBM-206</b></dt>
     * 
     * Verify that given valid criteria and the Database encounters an error,
     * then an empty array list of PanelistProfile should be returned.
     */
    @SuppressWarnings( "unchecked" )
    @Test
    public final void getPanelist_DBErrorOccurs_returnsNoResults () throws Exception {
        /* Arrange */
        criteria.put( "ISCID", "1001" );

        Connection mConnection = JDBCHelper.mockDBConnection();
        when( mConnection.createStatement() ).thenThrow( SQLException.class );
        when( PanelistContainer.getPanelists( criteria ) ).thenCallRealMethod();
        when( PanelistContainer.getAllPanelists() ).thenCallRealMethod();

        /* Act */
        ArrayList<PanelistProfile> result = DBManager.getPanelists( criteria );

        /* Assert */
        assertEquals( 0, result.size() );
    }

    /**
     * <dt><b>Test Case UTC-11/DBM-207</b></dt>
     * 
     * Verify that given valid criteria and the Database contains no Panelists,
     * then an empty array list of PanelistProfile should be returned.
     */
    @Test
    public final void getPanelist_noPanelistsInDB_returnsNoResults () {
        /* Arrange */
        criteria.put( "ISCID", "1001" );

        PanelistProfileFixture.defaultProfileCollection( 0, datapool );

        when( PanelistContainer.getPanelists( criteria ) ).thenCallRealMethod();
        when( PanelistContainer.getAllPanelists() ).thenReturn( datapool );

        /* Act */
        ArrayList<PanelistProfile> result = DBManager.getPanelists( criteria );

        /* Assert */
        assertEquals( 0, result.size() );
    }

}
