package Storage;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import Storage.Repository.DBConnection;
import edu.fiu.test.Helpers.JDBCHelper;

/**
 * == DBManager.createPanel Test Cases ==
 * 
 * Test cases based on requirements and specifications described in the ISC
 * documentation.
 * 
 * <ul>
 * <li> User Manual, Page 11, Create a Panel</li>
 * <li> Final Document, Page 15, Create Panel Functional Requirements</li>
 * <li> Final Document, Page 21, Create Panel Non-Function Requirements</li>
 * <li> Final Document, Page 35, Storage Tier Subsystem Decomposition</li>
 * <li> Final Document, Page 37, Panels Persistent Data Management</li>
 * <li> Final Document, Page 41, Panel Class Description</li>
 * <li> Final Document, Page 49, Create Panel Sequence Diagram</li>
 * <li> Final Document, Page 52, PanelContainer Detailed Class Desc.</li>
 * <li> Final Document, Page 52, Panel Detailed Class Desc.</li>
 * <li> Final Document, Page 76, Use Case ISCUC-09 Create Panel</li>
 * </ul>
 * 
 * <dt><b>Note:</b></dt> Assumes: {@link DBConnection} connects with a valid
 * connection _therefore_ tests will use a mocked connection.
 */

@PrepareForTest( { DBConnection.class, JDBCHelper.class } )
public class DBManagerCreatePanelTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private Connection mConnection;
    private Statement mStatement;
    private String panelName;
    private String panelDescription;
    private int employeeID;

    /**
     * Intercept max(panelid) from panel queries and mock the behavior.
     * 
     * @param max - the max to return in the ResultSet
     * @param mStatement - the Statement to bind the mock ResultSet to
     * @return - the mocked ResultSet
     */
    private final ResultSet mockSelectMaxQuery ( int max, Statement mStatement ) throws Exception {
        ResultSet mResultSet = mock( ResultSet.class );

        when( mStatement.executeQuery( "SELECT MAX(PanelID) FROM panel" ) ).thenReturn( mResultSet );
        when( mResultSet.getInt( "MAX(PanelID)" ) ).thenReturn( max );

        return mResultSet;
    }

    @Before
    public void setUp () throws Exception {
        mConnection = JDBCHelper.mockDBConnection();
        mStatement = JDBCHelper.mockDBStatement( mConnection );
        panelName = "life the universe and everything";
        panelDescription = "answering the question to life";
        employeeID = 42;
    }

    /**
     * <dt><b>Test Case UTC-16/DBM-401</b></dt>
     *
     * Verify that a panel can be created successfully.
     */
    @Test
    public final void createPanel_panelCreated_returnsTrue () throws Exception {
        /* Arrange */
        mockSelectMaxQuery( 1, mStatement );

        /* Assert */
        boolean result = DBManager.createPanel( panelName, panelDescription, employeeID );

        /* Act */
        assertTrue( result );
    }

    /**
     * <dt><b>Test Case UTC-17/DBM-402</b></dt>
     *
     * Verify that when a panel cannot be created then
     * false is returned to the caller.
     */
    @SuppressWarnings( "unchecked" )
    @Test
    public final void createPanel_panelFailsToSave_returnsFalse () throws Exception {
        /* Arrange */
        mockSelectMaxQuery( 1, mStatement );
        when( mStatement.executeUpdate( contains( "INSERT INTO panel" ) ) ).thenThrow( SQLException.class );

        /* Assert */
        boolean result = DBManager.createPanel( panelName, panelDescription, employeeID );

        /* Act */
        assertFalse( "should return false when panel insertion fails", result );
    }

}
