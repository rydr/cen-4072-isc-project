package Storage;

import static org.junit.Assert.*;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import Storage.Repository.DBConnection;
import edu.fiu.test.Helpers.JDBCHelper;

/**
 * == DBManager.updatePanelStatus Test Cases ==
 *
 * Test cases based on requirements and specifications described in the ISC
 * documentation.
 *
 * <ul>
 * <li> User Manual, Page 18, Update Panel Status and History</li>
 * <li> Final Document, Page 16, Update Panel Status and History Func. Req's</li>
 * <li> Final Document, Page 24, Update Panel Status and History Non-Fun. Req's</li>
 * <li> Final Document, Page 41, Panel Status Class Description</li>
 * <li> Final Document, Page 45, Update Panel Status Sequence Diagram</li>
 * <li> Final Document, Page 52, PanelContainer Storage Class Description</li>
 * <li> Final Document, Page 52, PanelStatus Storage Class Description</li>
 * <li> Final Dcoument, Page 97, Use Case ISCUC-17 Update Panel Status and Hist</li>
 * </ul>
 *
 * <dt><b>Notes</b></dt> Assumes {@link DBConnection} connects with a valid
 * connection _therefore_ tests will use a mocked connection.
 *
 * @todo make a test for unsigned ints on panelID
 */

@PrepareForTest( { DBConnection.class, JDBCHelper.class } )
public class DBManagerUpdatePanelStatusTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private Connection mConnection;
    private Statement mStatement;
    private String theStatus;
    private String comments;
    private int panelID;

    @Before
    public void setUp () throws Exception {
        mConnection = JDBCHelper.mockDBConnection();
        mStatement = JDBCHelper.mockDBStatement( mConnection );

        theStatus = "Active";
        comments = "42. That's the answer.";
        panelID = 42;
    }

    /**
     * <dt><b>Test Case UTC-21/DBM-601</b></dt>
     *
     * Verifies that given panel id exists, when the Panel Status is updated
     * successfully, then true is returned to the caller.
     */
    @Test
    public final void updatePanelStatus_PanelStatusUpdated_ReturnsTrue () throws Exception {
        /* Act */
        boolean result = DBManager.updatePanelStatus( theStatus, comments, panelID );

        /* Assert */
        assertTrue( "should return true when panel status is updated", result );
    }

    /**
     * <dt><b>Test Case UTC-22/DBM-602</b></dt>
     *
     * Verifies that given the panel id exists, when the Panel Status is not
     * updated due to errors at the database level and SQLException is thrown,
     * then false is returned.
     */
    @SuppressWarnings( "unchecked" )
    @Test
    public final void updatePanelStatus_PanelStatusNotUpdated_ReturnsFalse () throws Exception {
        /* Arrange */
        when( mStatement.executeUpdate( contains( "INSERT INTO panelhistory" ) ) ).thenThrow( SQLException.class );

        /* Act */
        boolean result = DBManager.updatePanelStatus( theStatus, comments, panelID );

        /* Assert */
        assertFalse( "should return false when panel status update exception thrown", result );
    }

}
