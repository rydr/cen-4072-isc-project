package Storage;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import Storage.Repository.DBConnection;
import Storage.Repository.DoubleRegistrationException;

import edu.fiu.test.Fixtures.PanelistProfileFixture;
import edu.fiu.test.Helpers.JDBCHelper;

/**
 * == DBManager.createPanelist Test Cases ==
 * 
 * Test cases based on requirements and specifications described in the ISC
 * documentation.
 * 
 * <dt><b>References:</b></dt>
 * <ul>
 * <li>User Manual, Page 7, Create Account for Panelist</li>
 * <li>Final Document, Page 14, Requirement Create Panelist Account</li>
 * <li>Final Document, Page 37, ISCID</li>
 * <li>Final Document, Page 42, Create Panelist Account Sequence</li>
 * <li>Final Document, Page 53, DBManager Class Description</li>
 * <li>Final Document, Page 53, PanelistContainer Class Description</li>
 * <li>Final Document, Page 60, Create Account for Panelist</li>
 * <li>Final Document, Page 107, Use Case ISCUC-21 Panelist Attempts to Register
 * Twice</li>
 * </ul>
 * 
 * <dt><b>Note:</b></dt> Assumes {@link DBConnection} connects with a valid
 * connection _therefore_ tests will use a mocked connection.
 * 
 */

@PrepareForTest( { DBConnection.class } )
public class DBManagerCreatePanelistTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    private HashMap<String, String> panelist;
    private Connection mConnection;

    @Before
    public void setUp () throws Exception {
        panelist = PanelistProfileFixture.defaultCreationHash();
        mConnection = JDBCHelper.mockDBConnection();
    }

    /**
     * <dt><b>Test Case UTC-01/DBM-101</b></dt>
     * 
     * Verify that a panelist cannot be created if an ISCID does not exist in
     * the database.
     */
    @Test
    public final void createPanelist_PanelistISCIDDoesNotExist_ReturnsFalse () throws Exception {
        /* Arrange */
        Statement mStatement = JDBCHelper.mockDBStatement( mConnection );
        JDBCHelper.mockSelectCountQuery( "iscids", 0, mStatement );

        /* Act */
        boolean result = DBManager.createPanelist( panelist );

        /* Assert */
        assertFalse( "createPanelist should return false when the ISCID does not exist", result );
    }

    /**
     * <dt><b>Test Case UTC-02/DBM-102</b></dt>
     * 
     * Verify that a DoubleRegistrationException is thrown if the count for
     * `iscpanelist.iscid == panelist.get("pISCID")` is greater than 0
     */
    @Test( expected = DoubleRegistrationException.class )
    public final void createPanelist_PanelistAlreadyRegistered_ThrowsDoubleRegistrationException () throws Exception {
        /* Arrange */
        Statement mStatement = JDBCHelper.mockDBStatement( mConnection );
        JDBCHelper.mockSelectCountQuery( "iscids", 1, mStatement );
        JDBCHelper.mockSelectCountQuery( "iscpanelist", 1, mStatement );

        /* Act */
        DBManager.createPanelist( panelist );

        /* Assert */
        // should throw DoubleRegistrationException (see @Test annotation)
    }

    /**
     * <dt><b>Test Case UTC-03/DBM-103</b></dt>
     * 
     * Verify that a panelist will be created if the ISCID is valid and not
     * already registered.
     */
    @Test
    public final void createPanelist_PanelistISCIDValid_ReturnsTrue () throws Exception {
        /* Arrange */
        Statement mStatement = JDBCHelper.mockDBStatement( mConnection );
        JDBCHelper.mockSelectCountQuery( "iscids", 1, mStatement );
        JDBCHelper.mockSelectCountQuery( "iscpanelist", 0, mStatement );
        JDBCHelper.mockSelectMaxQuery( "iscusers", "id", 1, mStatement );

        /* Act */
        boolean result = DBManager.createPanelist( panelist );

        /* Assert */
        assertTrue( "createPanelist should return true when ISCID is valid", result );
    }

    /**
     * <dt><b>Test Case UTC-04/DBM-104</b></dt>
     * 
     * Verify that database exceptions are handled.
     */
    @Test
    @SuppressWarnings( "unchecked" )
    public final void createPanelist_JDBCException_ReturnsFalse () throws Exception {
        /* Arrange */
        when( mConnection.createStatement() ).thenThrow( SQLException.class );

        /* Act */
        boolean result = DBManager.createPanelist( panelist );

        /* Assert */
        assertFalse( "createPanelist should handle internal DB Exceptions", result );
    }

}
