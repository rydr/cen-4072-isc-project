# README #

This project is for FIU's CEN 4072 Software Testing Fundamentals course. The goal of this project is to test a previous Software Engineering course project. Most (if not all) changes to this project should be contained within the `test/` directory.